
const express = require('express');
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const apiURL = 'https://api.aws.kambicdn.com/offering/api/v2/ub/event/live/open.json';

app.get("/", function(req, res) {
  getContent(apiURL)
    .then( (data) => {
        res.send(data);
    })
    .catch( (err) => {
        res.send(err);
    });
  
});






const getContent = function(url) {
  
  return new Promise((resolve, reject) => {
  
    const lib = url.startsWith('https') ? require('https') : require('http');
    const request = lib.get(url, (response) => {
 
      if (response.statusCode < 200 || response.statusCode > 299) {
         reject('Failed to load page, status code: ' + response.statusCode);
       }

      const body = [];

      response.on('data', (chunk) => body.push(chunk));
      response.on('end', () => resolve(body.join('')));
	  
    });
    request.on('error', (err) => reject(err))
    })
};




var server = app.listen(8080, function () {
  console.log("app running on port.", server.address().port);
});





